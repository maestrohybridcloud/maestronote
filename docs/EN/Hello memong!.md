# Hello memong!
Documentation for **How to use memong**.

visit [Main Showcase](http://memong.xyz/)

> **NOTE**: This site supports browsers - IE 11+, Chrome, Safari, Firefox.

## Login
Can be login by **Facebook** and **Google** account.

Your documents will be store in your account's database.


## Notes
Notes are synced with your account. 

### Create a note

### Switch to another note

### Rename a note

### Delete a note

### Editing a note


## Shortcut Key
<kbd>Shift+Enter</kbd> : Complete ``memo`` editing.

<kbd>TAB</kbd> : Focus move to down ``memo``.

<kbd>Arrow Up</kbd> : Focus move to up ``memo``.

<kbd>Arrow Down</kbd> : Focus move to down ``memo``.



## Markdown Grammers

